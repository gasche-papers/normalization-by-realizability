module SystemL where

open import Function

open import Data.Empty
open import Data.Unit
open import Data.Nat
open import Data.Fin
open import Data.Product
open import Data.Vec hiding (_∈_ ; module _∈_)

open import Relation.Nullary
open import Relation.Binary.PropositionalEquality

-- * (Untyped) Abstract machine

mutual
  data Comm (n : ℕ) : Set where
    ⟨_∣_⟩ : (t : Term n)(π : Stack n) → Comm n

  data Term (n : ℕ) : Set where
    λam : ScopeT n → Term n
    var : Fin n → Term n
    μ⋆ : Comm n → Term n

  data ScopeT (n : ℕ) : Set where
    bind : Term (suc n) → ScopeT n

  data Stack (n : ℕ) : Set where
    ⋆ : Stack n
    _∙_ : (t : Term n)(π : Stack n) → Stack n
    
COMM = Comm 0
TERM = Term 0
STACK = Stack 0

-- ** Operational semantics

Subst : (ℕ → Set) → ℕ → ℕ → Set
Subst T m n = Fin m → (T n)

mutual
  mapT : ∀{m n} → Subst Fin m n → Term m → Term n
  mapT ρ (λam b) = λam (mapScopeT ρ b)
  mapT ρ (var k) = var (ρ k)
  mapT ρ (μ⋆ ⟨ t ∣ π ⟩) = μ⋆ ⟨ mapT ρ t ∣ mapS ρ π ⟩

  weaken : ∀{m n} → Subst Fin m n → Subst Fin (suc m) (suc n)
  weaken ρ zero = zero 
  weaken ρ (suc k) = suc (ρ k)

  mapScopeT : ∀{m n} → Subst Fin m n → ScopeT m → ScopeT n
  mapScopeT ρ (bind t) = bind (mapT (weaken ρ) t)

  mapS : ∀{m n} → Subst Fin m n → Stack m → Stack n
  mapS ρ ⋆ = ⋆
  mapS ρ (t ∙ π) = mapT ρ t ∙ mapS ρ π

mutual
  subT : ∀{m n} → Subst Term m n → Term m → Term n
  subT ρ (λam b) = λam (subScopeT ρ b)
  subT ρ (var k) = ρ k
  subT ρ (μ⋆ ⟨ t ∣ π ⟩) = μ⋆ ⟨ subT ρ t ∣ subS ρ π ⟩

  rename : ∀{m n} → Subst Term m n → Subst Term (suc m) (suc n)
  rename ρ zero = var zero 
  rename ρ (suc k) = mapT suc (ρ k)

  subScopeT : ∀{m n} → Subst Term m n → ScopeT m → ScopeT n
  subScopeT {m}{n} ρ (bind t) = bind (subT (rename ρ) t)

  subS : ∀{m n} → Subst Term m n → Stack m → Stack n
  subS ρ ⋆ = ⋆
  subS ρ (t ∙ π) = subT ρ t ∙ subS ρ π

_∘ST_ : ∀{l m n} → Subst Term m n → Subst Term l m → Subst Term l n
τ ∘ST ρ = λ k → subT τ (ρ k)


sub0 : ∀{n} → Term n → Subst Term (suc n) n
sub0 tm zero = tm 
sub0 tm (suc k) = var k 

sub : ∀{n} → ScopeT n → Term n → Term n
sub (bind b) tm = subT (sub0 tm) b 

sub⋆ : ∀{n} → Stack n → Stack n → Stack n
sub⋆ ⋆ π' = π'
sub⋆ (t ∙ π) π' = t ∙ (sub⋆ π π')

data _↝_ : COMM → COMM → Set where
  app : ∀{b u π} →
    ⟨ λam b ∣ u ∙ π ⟩ ↝ ⟨ sub b u ∣ π ⟩
  com : ∀{t π π'} →
    ⟨ (μ⋆ ⟨ t ∣ π ⟩) ∣ π' ⟩ ↝ ⟨ t ∣ sub⋆ π π' ⟩

data _↝*_ : COMM → COMM → Set where
  ε : ∀{c} → ¬ (∃ λ c' → c ↝ c') → c ↝* c
  step : ∀{c c' c''} →
    c ↝ c' → c' ↝* c'' → c ↝* c''
          
-- * Propositional logic, in sequent form

infixr 40 _`→_
data Type : Set where
--  Unit : Type
  _`→_ : (S T : Type) → Type


infixl 30 _▸_
data Ctxt : Set where
  ε : Ctxt
  _▸_ : (Γ : Ctxt)(T : Type) → Ctxt

infix 15 _∈_
data _∈_ : Type → Ctxt → Set where
  zero : ∀{T Γ} → T ∈ Γ ▸ T
  suc : ∀{T T' Γ} → T ∈ Γ → T ∈ Γ ▸ T'


mutual
  data _⊢⋆∶_ (Γ : Ctxt)(T : Type) : Set where
    ⟨_∣_⟩ : ∀{A} → (t : Γ ⊢ A)(π : Γ ∶ A ⊢ T) → Γ ⊢⋆∶ T 

  data _⊢_ (Γ : Ctxt) : Type → Set where
    λam : ∀{A B} → 

        Γ ▸ A ⊢ B →
        -----------
        Γ ⊢ A `→ B

    var : ∀{A} →

        A ∈ Γ →
        --------
        Γ ⊢ A

  data _∶_⊢_ (Γ : Ctxt) : Type → Type → Set where
    var : ∀{A} →

        Γ ∶ A ⊢ A

    app : ∀{A B C} →
  
        Γ ⊢ A →
        Γ ∶ B ⊢ C →
        Γ ∶ A `→ B ⊢ C

-- * Realizer extraction

⌊_⌋ctxt : Ctxt → ℕ
⌊ ε ⌋ctxt = 0
⌊ Γ ▸ _ ⌋ctxt = suc ⌊ Γ ⌋ctxt

⌊_⌋∈ : ∀{Γ T} → T ∈ Γ → Fin ⌊ Γ ⌋ctxt
⌊ zero ⌋∈ = zero
⌊ suc k ⌋∈ = suc ⌊ k ⌋∈

mutual
  ⌊_⌋c : ∀{Γ T} → Γ ⊢⋆∶ T → Comm ⌊ Γ ⌋ctxt
  ⌊ ⟨ t ∣ π ⟩ ⌋c = ⟨ ⌊ t ⌋t ∣ ⌊ π ⌋s ⟩

  ⌊_⌋t : ∀{Γ T} → Γ ⊢ T → Term ⌊ Γ ⌋ctxt
  ⌊ λam b ⌋t = λam (bind (⌊ b ⌋t))
  ⌊ var k ⌋t = var ⌊ k ⌋∈

  ⌊_⌋s : ∀{Γ S T} → Γ ∶ S ⊢ T → Stack ⌊ Γ ⌋ctxt
  ⌊ var ⌋s = ⋆
  ⌊ app t π ⌋s = ⌊ t ⌋t ∙ ⌊ π ⌋s 

-- * Realizability predicates

∐ : COMM → Set
∐ c = ∃ λ v → c ↝* v 

mutual
  [_]T : Type → TERM → Set
  [ T ]T t = ∀ {⌊π⌋ : STACK}(π : [ T ]S ⌊π⌋) → ∐ ⟨ t ∣ ⌊π⌋ ⟩ 

  [_]S : Type → STACK → Set 
--  [ Unit ]S π' = {!!}
  [ S `→ T ]S π' = Σ[ u ∈ TERM ] Σ[ π ∈ STACK ] π' ≡ u ∙ π × 
                    [ S ]T u × [ T ]S π


-- ** Adequacy theorem:

[_]env : (Γ : Ctxt) → Subst Term ⌊ Γ ⌋ctxt 0 → Set
[ ε ]env ρ = ⊤
[ Γ ▸ T ]env ρ = [ T ]T (ρ zero) × [ Γ ]env (λ k → ρ (suc k))

_∘ST0_ : ∀{m n} → Term n → Subst Term m n → Subst Term (suc m) n
u ∘ST0 ρ = λ { zero → u ; (suc k) → ρ k }


lemma-subT-mapT : ∀{l m n}(ρ : Subst Term m n)(τ : Subst Fin l m)(t : Term l) →
   subT ρ (subT (var ∘ τ) t) ≡ subT ρ (mapT τ t)
lemma-subT-mapT = {!!}

lemma-subT-left : ∀{n}(t : Term n) →  subT var t ≡ t
lemma-subT-left = {!!}

lemma-subT-assoc : ∀{l m n}(ρ : Subst Term m n)(τ : Subst Term l m)(t : Term l) →
   subT (subT ρ ∘ τ) t
     ≡
   subT ρ (subT τ t)
lemma-subT-assoc = {!!}

lemma-sub0-rename : ∀{m n} → (u : Term n)(ρ : Subst Term m n)(k : Fin (suc m)) → (u ∘ST0 ρ) k ≡ subT (sub0 u) (rename ρ k)
lemma-sub0-rename u ρ zero = refl
lemma-sub0-rename u ρ (suc k) = begin
     (u ∘ST0 ρ) (suc k)
   ≡⟨ refl ⟩
     ρ k
   ≡⟨ sym (lemma-subT-left (ρ k)) ⟩
     subT var (ρ k)
   ≡⟨ refl ⟩
     subT (λ k → subT (sub0 u) (var (suc k))) (ρ k)
   ≡⟨ lemma-subT-assoc (sub0 u) (λ k → var (suc k)) (ρ k) ⟩
     subT (sub0 u) (subT (λ k → var (suc k)) (ρ k))
   ≡⟨ lemma-subT-mapT (sub0 u) suc (ρ k) ⟩
     subT (sub0 u) (mapT suc (ρ k))
   ≡⟨ refl ⟩
     subT (sub0 u) (rename ρ (suc k))
   ∎
        where open ≡-Reasoning

subCtxt : ∀{n} → Comm n → Subst Term n 0 → STACK → COMM
subCtxt ⟨ t ∣ π ⟩ ρ π' = ⟨ subT ρ t ∣ sub⋆ (subS ρ π) π' ⟩ 

mutual
  evalC : ∀{Γ T ρ π} → (c : Γ ⊢⋆∶ T) → [ Γ ]env ρ → [ T ]S π → ∐ (subCtxt (⌊ c ⌋c) ρ π)
  evalC {ρ = ⌊ρ⌋}{π = ⌊π⌋} ⟨ t ∣ π ⟩ ρ π' = evalT t ρ  (evalS π ρ π')

  evalT : ∀{Γ T ⌊ρ⌋} → (t : Γ ⊢ T) → [ Γ ]env ⌊ρ⌋ → [ T ]T (subT ⌊ρ⌋ ⌊ t ⌋t)
  evalT (var zero) (t , _) π = t π
  evalT (var (suc x)) (_ , ρ) = evalT (var x) ρ
  evalT {Γ}{A `→ B}{⌊ρ⌋} (λam b) ρ = λ { (⌊t⌋ , ⌊π'⌋ , q , t , π') → 
                      let res : _
                          res = (evalT {Γ ▸ A}{B}{subT (sub0 ⌊t⌋) ∘ rename ⌊ρ⌋} b 
                                (t , subst [ Γ ]env (cong (λ f → f ∘ suc) (ext (lemma-sub0-rename ⌊t⌋ ⌊ρ⌋))) ρ)
                                {⌊π'⌋} π') 
                      in
                      subst (λ ⌊π⌋ → ∐ ⟨ subT ⌊ρ⌋ (λam (bind ⌊ b ⌋t)) ∣ ⌊π⌋ ⟩) (sym q) 
                            ((proj₁ res , step app (subst (λ t → ⟨ t ∣  ⌊π'⌋ ⟩ ↝* proj₁ res)
                                                          (lemma-subT-assoc (sub0 ⌊t⌋) (rename ⌊ρ⌋) ⌊ b ⌋t) 
                                                          (proj₂ res)))) }

        where postulate ext : Extensionality _ _

  evalS : ∀{Γ S T ρ π'} → (π : Γ ∶ S ⊢ T) → [ Γ ]env ρ → [ T ]S π' → [ S ]S (sub⋆ (subS ρ ⌊ π ⌋s) π')
  evalS var ρ π' = π'
  evalS {ρ = ⌊ρ⌋}{π' = ⌊π'⌋} (app t π) ρ π' = subT ⌊ρ⌋ ⌊ t ⌋t , sub⋆ (subS ⌊ρ⌋ ⌊ π ⌋s) ⌊π'⌋ , refl , evalT t ρ , evalS π ρ π'
