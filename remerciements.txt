Nous avons eu le plaisir de discuter avec Guillaume Munch-Maccagnoni,
Jean-Philippe Bernardy, Hugo Herbelin et Marc Lasson; non contents
d'avoir diffusé les idées au cœur de ce travail, ils se sont montrés
disponibles et pédagogues. Enfin, la relecture d'Adrien Guatto
a apporté un œil extérieur indispensable.
